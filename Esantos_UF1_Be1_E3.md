**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

**Exercici 1:** 

**Escriu un missatge de benvinguda per pantalla.** 

![](1.png)

___

**Exercici 2:** 

**Escriu un enter, un real, i un caràcter per pantalla.** 

![](2.png)

___

**Exercici 3:** 

**Demana dos enters per teclat i mostra la suma per pantalla.** 

![](3.png)

___

**Exercici 4:** 

**Indica el tipus de dades que correspon en cada cas:**

**·Edat.** L’edat s’indca en tipus enter.

**·Temperatura.** La tempratura s’expresa en format float o també double.

**·La resposta a la pregunta: Ets solter (s/n)?** 

És una dada booleana i la indicarem amb un si o un no.

**·El resultat d’avaluar la següent expressió: (5>6 o 4<=8)**  

És una dada booleana la expresarem com a true o false.

**·El teu nom.** 

És un conjunt de dades, cadena o string.

___

**Exercici 5:** 

**Mostrar per pantalla els 5 primers nombres naturals.** 

![](5.png)

___

**Exercici 6:** 

**Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.** 

![](6.png)

___

**Exercici 7:** 

**Intercanvi. Demana dos enters i intercanvia els valors de les variables.** 

![](7.png)

___

**Exercici 8:** 

**Aquest exercici només en C. Mostrar per pantalla el següent menú:**

Opcions:   
&emsp;&emsp;&emsp;1-Alta  
&emsp;&emsp;&emsp;2-Baixa  
&emsp;&emsp;&emsp;3-Modificacions      
&emsp;&emsp;&emsp;4-Sortir  

Tria opció (1-4):  

![](8.png)

___

**Exercici 9:** 

**Mostra per pantalla el següent menú:**  

&emsp;\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
&emsp;\*\*\*\*&nbsp;&emsp;&emsp;        MENU&nbsp;&ensp;&emsp;       \*\*\*\*  
&emsp;\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
&emsp;\*\*\*\*&nbsp;&emsp;&emsp;      1-alta&emsp;&emsp;          \*\*\*\*  
&emsp;\*\*\*\*&emsp;&emsp;      2-baixa&emsp;&ensp;       \*\*\*\*  
&emsp;\*\*\*\*&emsp;&emsp;      3-sortir&emsp;&ensp;        \*\*\*\*   
&emsp;\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  



 ![](9.png)
___

**Exercici 10: Feu l’algorisme que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès simples, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el rèdit (r). La fórmula per obtenir el CF a interès simples és:**

![](10.1.png)

**Donat que el temps està expressat en anys la k=1.**  

**El rèdit ‘r’ ve donat en %.** 

**Nota: les dades de l’exercici cal demanar-les per teclat.**  

![](10.2.png)

___

**Exercici 11: Feu l’algorisme que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès compost, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el interès (i). La fórmula per obtenir el CF a interès compost és:**

![](11.1.png)

**L’ interès ‘i’ ve donat en tant per 1.** 

**Nota: les dades de l’exercici cal demanar-les per teclat.**

![](11.2.png) ![](11.3.png)

___

**Exercici 12: Feu un programa que permeti generar una travessa de forma aleatòria. Tot seguit ha de permetre introduir-ne una per teclat i comprovar-ne els encerts respecte la travessa generada aleatòriament. El format de sortida queda a la vostra elecció sempre que n’informi dels encerts.**  
  
    
    
![](12.1.png)![](12.2.png)![](12.3.png)

___

**Exercici 13: Feu un programa que permeti simular el resultat de tirar un dau. Tot seguit ha de demanar un número per teclat, entre 1 i 6, i ens ha dir si és el mateix que el que hem generat aleatòriament.** 

![](13.png)
